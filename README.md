## About

<p>
    <b>Sifex</b> - stands for <b>Si</b>mple <b>F</b>ile <b>Ex</b>change.
    It allows you to send files across your LAN.
</p> 

## Features
>>>
  Transfering simple txt files ✔
 
  Transfering image(png/jpeg) files ✖ 
 
  Track all processes on Linux machine and write it into a file ✔

  Interactive interface ✖
>>>
