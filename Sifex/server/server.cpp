#include "server.hpp"

server::server()
    : maxCountOfSocketConnections{10}
{
    memset(&hints,0,sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    socketAddrStorageLength = sizeof(sockaddr);

    uint32_t receivingStatus = getaddrinfo(nullptr,port.c_str(),&hints,&serverInfo);

    if(receivingStatus != 0)
    {
        std::cout << "some errors occured during establishing listening\n";
    }



}

server::~server()
{
    receivedData.close();
    delete[] serverInfo;
    delete[] tmpServerInfo;
}

void server::selectAppropriateSocket()
{

    for(tmpServerInfo = serverInfo; tmpServerInfo != nullptr; tmpServerInfo=tmpServerInfo->ai_next)
    {

        socketfd = socket(tmpServerInfo->ai_family,tmpServerInfo->ai_socktype,tmpServerInfo->ai_protocol);
        if(socketfd == -1)
        {
            perror("server: socket");
            continue;
        }

        if (setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &socketOptVal, sizeof(int)) == -1)
        {
            perror("setsockopt");
            exit(1);
        } // deleting zombie processes

        if (bind(socketfd, tmpServerInfo->ai_addr, tmpServerInfo->ai_addrlen) == -1)
        {
            close(socketfd);
            perror("server: bind");
            continue;
        }

        break;
    }

    freeaddrinfo(serverInfo);

    if(tmpServerInfo == nullptr)
    {
        std::cout << "err to bind\n";
    }

    if(listen(socketfd,maxCountOfSocketConnections) == -1)
    {
        perror("listen");
        exit(1);
    }


}

void server::reapAllDeadProcesses()
{
    int32_t saved_errno = errno;

    while(waitpid(-1, nullptr, WNOHANG) > 0);

    errno = saved_errno;

    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;

    if(sigaction(SIGCHLD,&sa,nullptr) == -1)
    {
        perror("sigaction");
        exit(1);
    }

}

void server::establishConnetionAndReceiveData()
{
    while(true) // accepting connection
    {
        newSocketfd = accept(socketfd,reinterpret_cast<sockaddr*>(&connectedDeviceInfo),&socketAddrStorageLength);
        if(newSocketfd == -1)
        {
            perror("accept");
            continue;
        }


        inet_ntop(connectedDeviceInfo.ss_family,getSocketAddr(reinterpret_cast<sockaddr*>(&connectedDeviceInfo)),
                  senderIP,sizeof(senderIP));

        std::cout << "server: got connection from " << senderIP << "\n";

        close(socketfd);

        // ----------------  receiving data --------------
        char fileString[MAXDATASIZE];

        char filename[MAXDATASIZE];
        int tmpSizeOfreceivingData = recv(newSocketfd,filename,MAXDATASIZE,0);

        if(tmpSizeOfreceivingData == -1)
        {
            std::cout << "Client: filename transfer error\n";
            exit(1);
        }

        std::cout << "Filename: " << filename << "\n";


        std::ofstream newData( filename , std::ios::binary);

        if(!newData)
        {
            std::cout << "file wasn't open!\n";
            exit(-1);
        }

        while(true)
        {
            sizeOfreceivingData = recv(newSocketfd,fileString,MAXDATASIZE,0);

            if(sizeOfreceivingData == -1)
            {
                std::cout << "Client: transfer error\n";
                exit(1);
            }

            if(sizeOfreceivingData == 0)
            {
                std::cout << "transfering has ended.\n";
                newData.close();
                close(newSocketfd);
                exit(0);
            }



            fileString[sizeOfreceivingData] = '\0';

            std::cout << "server: received [" << fileString << "]  " << strlen(fileString) << "\" bytes\n";
            std::string tmps = fileString;
            newData << tmps << "\n";
        };

    }
}

void* server::getSocketAddr(sockaddr* sa) const
{
    if(sa->sa_family == AF_INET)
    {
        auto tmp = reinterpret_cast<sockaddr_in*>(sa);
        return &tmp->sin_addr;
    }
    else
    {
        auto tmp = reinterpret_cast<sockaddr_in6*>(sa);
        return &tmp->sin6_addr;
    }
}




