#include "ProcessTracker.hpp"

namespace Sifex::Service::ProcessTracking
{
//    ProcessTracker::ProcessTracker()
//        : outputFilePath("../../../client/")
//    {
//
//    }

    void ProcessTracker::readAllProcesses()
    {
        std::cout << "started scanning processes...\n";
        DIR* processDirectory{};
        dirent* directoryEntity{};
        int32_t directoryCounter{0};

        processDirectory = opendir(PROC_DIRECTORY.c_str());

        if(processDirectory == nullptr)
        {
            std::cerr << "directory was not found\n";
            exit(-1);
        }

        while(directoryEntity = readdir(processDirectory))
        {
            if(directoryCounter > 2) // start read after . and ..
            {
                if (directoryEntity->d_type != DT_DIR)
                {
                    continue;
                }
                else
                {
                    if(!std::atoi(directoryEntity->d_name))
                        continue;

                    uint16_t  processPID = std::atoi(directoryEntity->d_name);
                    //process dir
                    std::string directoryPath = PROC_DIRECTORY;
                    directoryPath += directoryEntity->d_name;

                    if (opendir(directoryPath.c_str()) == nullptr) {
                        std::cerr << "directory was not found\n";
                        continue;
                    }

                    // opening status file if it exist
                    directoryPath += "/status"; // going deeper to extract name of the process.
                    std::ifstream is(directoryPath , std::ios_base::binary);
                    char processName[256]{};
                    is.getline(processName, 256, '\n');

                    if (is)
                    {
                        // insert the pair if no same PID found
                        if(processMap.find(processPID) == processMap.end())
                        {
                            ::Sifex::ProcessTracking::Process temporaryProcess(processPID, &*(processName + 6));
                            processMap.insert(std::make_pair(processPID, temporaryProcess));
                        }
                        else
                        {
                            if(processMap.find(processPID)->second.getName().c_str() != processName)
                            {
                                processMap.find(processPID)->second.setName(&*(processName + 6)); // rewrite process name(could be a new process with existing in map PID)
                            }
                        }
                    }
                }
            }
            directoryCounter++;
        }
        closedir(processDirectory);
    }

    void ProcessTracker::showAllProcesses() const
    {
        for(auto& it : processMap)
        {
            std::cout << std::setw(4) << it.first << " " << std::setw(30) << it.second.getName() << " | " << it.second.showTimer() << std::endl;
        }
    }

    void ProcessTracker::showCertainProcesses(uint16_t _PID) const
    {
        auto process = processMap.find(_PID);
        if(process != processMap.end())
        {
            std::cout << std::setw(4) << process->first << " " << std::setw(30) << process->second.getName() << " | " << process->second.showTimer() << std::endl;
        }
    }

    void ProcessTracker::writeAllProcesses() const
    {
        std::ofstream os("output.txt");

        for(auto& it : processMap)
        {
            std::string infostring = std::to_string(it.first) + "::" + it.second.getName() + "\n";
            os <<  infostring;
        }
    }

    void ProcessTracker::setOutputFilePath(std::string _outputFilePath)
    {
        outputFilePath = std::move(_outputFilePath);
    }



}
