#include "Client.hpp"
#include "../services/ProcessTracking/ProcessTracker/ProcessTracker.hpp"

int main(int argc,char** argv)
{
    if(argc < 3)
    {
        std::cout << "not enough arguments\n";
        std::cout << "Pattern: [appname] [serverIP] [server Port] [Max data size]\n";
        return 1;
    }

    std::string serverIP{argv[1]};
    std::string serverPort{argv[2]};
    uint32_t maxSendingDataSize{};
    maxSendingDataSize = atoi(argv[3]);


    std::cout << serverIP << "\n";
    std::cout << serverPort << "\n";
    std::cout << maxSendingDataSize << "\n";


    Sifex::Client::Client client(serverIP.c_str(), serverPort.c_str(), maxSendingDataSize);
    client.selectAppropriateSocket();

    Sifex::Service::ProcessTracking::ProcessTracker Tracker;
    Tracker.readAllProcesses();
    Tracker.writeAllProcesses();

    client.sendData();
}