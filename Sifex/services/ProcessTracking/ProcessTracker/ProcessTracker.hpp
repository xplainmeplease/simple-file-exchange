#pragma once

#include "../ProcessEntity/Process.hpp"

#include <iostream>
#include <map>
#include <dirent.h>
#include <fstream>
#include <iomanip>


const std::string PROC_DIRECTORY = "/proc/";
namespace Sifex::Service::ProcessTracking
{
    class ProcessTracker
    {
    public:
        ProcessTracker() = default;
        ~ProcessTracker() = default;

        ProcessTracker(const ProcessTracker&) = delete;
        ProcessTracker(ProcessTracker&&)      = delete;

        ProcessTracker& operator=(const ProcessTracker&) = delete;
        ProcessTracker& operator=(ProcessTracker&&)      = delete;

        void setOutputFilePath(std::string _outputFilePath);

        void readAllProcesses();

        void showAllProcesses() const;
        void showCertainProcesses(uint16_t _PID) const;

        void writeAllProcesses() const;

    private:
        std::map<uint16_t ,::Sifex::ProcessTracking::Process> processMap;
        std::string outputFilePath;

    };
}

