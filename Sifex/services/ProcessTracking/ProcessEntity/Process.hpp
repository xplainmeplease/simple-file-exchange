#pragma once

#include <ctime>
#include <string>

namespace Sifex::ProcessTracking
{
    class Process
    {
    public:
        Process() = default;
        Process(uint16_t _pid, const std::string& _name);

        ~Process();

        std::string showTimer() const;

        uint16_t  getPID() const;
        const std::string& getName() const;
        const tm& getTimer() const;

        void setName(const std::string& _name);
        void setTimer(const tm& _timer);
    private:
        uint16_t  pid;
        std::string name;
        tm procTimer;
    };
}
