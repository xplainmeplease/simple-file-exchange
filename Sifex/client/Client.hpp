#pragma once


#include <netdb.h>
#include <cstring>
#include <iostream>
#include <unistd.h>
#include <arpa/inet.h>
#include <fstream>


namespace Sifex::Client
{
    class Client
    {
    public:
        Client(const char* _serverIP, const char*  _serverPort, uint32_t _maxSendingDataSize);
        ~Client();

        Client(const Client&) = delete;
        Client(Client&&)      = delete;

        Client& operator=(const Client&) = delete;
        Client& operator=(Client&&)      = delete;


        void selectAppropriateSocket();
        void* getSocketAddr(sockaddr* sa) const;
        void sendData();

    private:
        int32_t socketfd;
        int32_t maxSendingDataSize;

        char serverIP[INET6_ADDRSTRLEN];
        char serverPort[6];

        std::ifstream fileStream;

        addrinfo hints;
        addrinfo* serverInfo;
        addrinfo* tmpServerInfo; // used like iterator for serverInfo;
    };
}

