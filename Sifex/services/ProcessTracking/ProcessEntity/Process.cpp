#include "Process.hpp"


namespace Sifex::ProcessTracking
{
    Process::Process(uint16_t  _pid, const std::string& _name)
    : pid(_pid), name(_name)
    {
        time_t now = time(0);
        procTimer = *gmtime(&now);
    }

    Process::~Process()
    {

    }

    uint16_t  Process::getPID() const
    {
        return pid;
    }

    const std::string& Process::getName() const
    {
        return name;
    }

    const tm& Process::getTimer() const
    {
        return procTimer;
    }

    std::string Process::showTimer() const
    {
        std::string out =  "Process has started at : " + std::to_string(procTimer.tm_mday) + ":" + std::to_string(procTimer.tm_mon) + ":" + std::to_string(procTimer.tm_year + 1900) + " | " + std::to_string(procTimer.tm_hour) + ":" + std::to_string(procTimer.tm_min) +  ":" + std::to_string(procTimer.tm_sec);
        return out;
    }

    void Process::setName(const std::string &_name)
    {
        name = _name;
    }

    void Process::setTimer(const tm &_timer)
    {
        time_t now = time(0);
        procTimer = *gmtime(&now);
    }
}



