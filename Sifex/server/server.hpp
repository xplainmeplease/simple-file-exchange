#pragma once

#include <cstring>
#include <iostream>
#include <fstream>

#include <netdb.h>
#include <csignal>
#include <unistd.h>
#include <wait.h>
#include <arpa/inet.h>

#define MAXDATASIZE 100


class server
{
public:
     server();
    ~server();
    void selectAppropriateSocket();
    void reapAllDeadProcesses();
    void establishConnetionAndReceiveData();
    void* getSocketAddr(sockaddr* sa) const;

private:
    std::string port = "59999";

    int32_t maxCountOfSocketConnections;
    int32_t sizeOfreceivingData; // number of bytes server should receive
    int32_t socketfd; // socket to listen
    int32_t newSocketfd; // new connection
    int32_t socketOptVal;

    addrinfo hints;
    addrinfo* serverInfo;
    addrinfo* tmpServerInfo; // used like iterator for serverInfo;

    sockaddr_storage connectedDeviceInfo; // connected divice info;

    socklen_t socketAddrStorageLength; // sockadd_storage size;

    std::ofstream receivedData;

    struct sigaction sa; // for deleting zombie-processes after fork();

    char senderIP[INET6_ADDRSTRLEN];
};
