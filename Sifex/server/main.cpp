#include "server.hpp"

int main()
{
    server s;
    s.selectAppropriateSocket();
    s.reapAllDeadProcesses();
    s.establishConnetionAndReceiveData();
}